package util;

import java.util.Date;

public class DateUtil {
    static long dayPerTime = 86_400_000;

    public static long getNowDateTime() {
        return new Date().getTime();
    }
}
