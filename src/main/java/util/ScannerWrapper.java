package util;

import java.util.Scanner;

public final class ScannerWrapper {

    private static Scanner scanner=new Scanner(System.in);

    private ScannerWrapper() {
    }

//    public static Scanner getInstance() {
//        return INSTANCE.scanner;
//    }
public static int getInt(){
    return Integer.parseInt(scanner.nextLine());
}
    public static long getLong(){
        return Long.parseLong(scanner.nextLine());
    }
    public static String getLine(){
        return scanner.nextLine();
    }
//    public static String getLine(){
//        return scanner.nextLine();
//    }
}
