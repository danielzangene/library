import controller.UserController;
import model.Book;
import model.BorrowInfo;
import model.User;
import repository.LibraryRepository;
import util.DateUtil;
import util.ScannerWrapper;

import java.sql.Date;

public class Main {

    public static void main(String[] args) {
        while (true) {
            System.out.print("--->Library\n" +
                    "1.sign up.\n" +
                    "2.login.\n" +
                    "num ?");
            int item =ScannerWrapper.getInt();
            switch (item) {
                case 1:
                    UserController.SignUp();
                    break;
                case 2:
                    UserController.login();
                    break;
                default:
                    continue;
            }
        }
    }
}
