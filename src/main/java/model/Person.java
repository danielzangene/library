package model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public abstract class Person extends BaseEntity {
    private String name;
    private String nationalCode;
    private Timestamp createdAt;
    private String password;
    private String email;

    public Person(String name, String nationalCode, Timestamp createdAt, String password, String email) {
        this.name = name;
        this.nationalCode = nationalCode;
        this.createdAt = createdAt;
        this.password = password;
        this.email = email;
    }
}
