package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BaseEntity{
    private long id;
}
