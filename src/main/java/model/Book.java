package model;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Setter
@Getter
public class Book extends BaseEntity {
    private String title;
    private String subject;
    private long isbn;
    private Date status;
    private long price;
    private long penaltyFee;

    public Book(String title, String subject, long isbn, Date status, long price, long penaltyFee) {
        this.title = title;
        this.subject = subject;
        this.isbn = isbn;
        this.status = status;
        this.price = price;
        this.penaltyFee = penaltyFee;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", subject='" + subject + '\'' +
                ", isbn=" + isbn +
                ", status=" + status +
                ", price=" + price +
                ", penaltyFee=" + penaltyFee +
                '}';
    }
}
