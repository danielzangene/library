package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@AllArgsConstructor
@Getter
@Setter
public class BorrowInfo extends BaseEntity {

    private Long userId;
    private Long bookId;
    private Date start;
    private Date end;
    private Date returnDate;
    private long penalty;
    //86,400,000 day / time

    public BorrowInfo(Long userId, Long bookId) {
        this.userId = userId;
        this.bookId = bookId;
    }

    @Override
    public String toString() {
        return "BorrowInfo{" +
                "id=" + getId() +
                ",userId=" + userId +
                ", bookId=" + bookId +
                ", start=" + start +
                ", end=" + end +
                ", returnDate=" + returnDate +
                ", penalty=" + penalty +
                '}';
    }
}
