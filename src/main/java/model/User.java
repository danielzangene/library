package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;
import java.sql.Timestamp;

@Getter
@Setter
public class User extends Person {
    private Date status;
    private long budget;
    private byte incorrectPass;
    private byte getLate;


    public User(String name, String nationalCode, Timestamp createdAt, String password, String email, Date status, long budget) {
        super(name, nationalCode, createdAt, password, email);
        this.status = status;
        this.budget = budget;
    }

    public User(String name, String nationalCode, Timestamp createdAt, String password, String email, Date status, long budget, byte incorrectPass, byte getLate) {
        super(name, nationalCode, createdAt, password, email);
        this.status = status;
        this.budget = budget;
        this.incorrectPass = incorrectPass;
        this.getLate = getLate;
    }

    @Override
    public String toString() {
        return "User{" +
                "id: " + getId() +
                ",name: " + getName() +
                ",password: " + getPassword() +
                ",email: " + getEmail() +
                ",national code: " + getNationalCode() +
                ",create at: " + getCreatedAt() +
                ",status: " + status +
                ", budget: " + budget +
                ", incorrectPass: " + incorrectPass +
                ", getLate: " + getLate +
                "}\n";
    }
}
