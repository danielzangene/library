package repository.connection;

import java.sql.Connection;

public class ConnectionManager {
    private Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }
}
