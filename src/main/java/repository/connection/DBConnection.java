package repository.connection;

import java.sql.Connection;

public abstract class DBConnection {

    public abstract Connection Connection();
}
