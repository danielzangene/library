package repository;

import java.util.Collection;

public interface Repository<T,ID> {
    int insert(T t);

    T search(ID id);

    int update(T t);

    int delete(ID id);

    Collection<T> findAll();
    int custem(String query);
}
