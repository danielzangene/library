package repository;

import model.Book;
import model.BorrowInfo;
import model.User;
import repository.connection.MySQLConnection;
import repository.dao.BookDAO;
import repository.dao.BorrowDAO;
import repository.dao.UserDAO;

import java.sql.Connection;
import java.sql.DriverManager;

public class LibraryRepository {
//    private static Connection connection = MySQLConnection.getCONNECTION();
    public static UserDAO userRepo = UserDAO.INSTANCE;
    public static BookDAO bookRepo = BookDAO.INSTANCE;
    public static BorrowDAO borrowInfoRepo = BorrowDAO.INSTANCE;
}
