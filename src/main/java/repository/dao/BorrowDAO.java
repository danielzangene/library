package repository.dao;

import model.BorrowInfo;
import repository.connection.MySQLConnection;

import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;

public class BorrowDAO extends AbstractDAO<BorrowInfo, Long> {

    public static BorrowDAO INSTANCE = new BorrowDAO();
    private String insertQuery = "INSERT INTO `borrows`(`id`, `user_id`, `book_isbn`, `start`, `end`) VALUES" +
            " (NULL,?,?,?,?)";
    private String searchQuery = "select * from borrows where id =?";
    private String updateQuery = "UPDATE `borrows` SET `user_id`=?,`book_isbn`=?,`start`=?,`end`=?,`return_date`=?,`penalty`=? WHERE id=?";
    private String deleteQuery = "DELETE FROM `borrows` WHERE id=?";
    private String selectAllQuery = "select * from borrows ";
    private String selectAllWithIsbnQuery = "SELECT * FROM borrows WHERE book_isbn=? ORDER BY id DESC LIMIT 1";

    private BorrowDAO() {
    }

    static {
        INSTANCE.setConnection(MySQLConnection.getCONNECTION());
    }

    @Override
    public int insert(BorrowInfo borrow) {
//        private String updateQuery = "UPDATE `borrows` SET `user_id`=?,`book_isbn`=?,`start`=?,`end`=?,`return_date`=?,`penalty`=? WHERE id=?";

        try {
            PreparedStatement statement = getConnection().prepareStatement(insertQuery);
            statement.setLong(1, borrow.getUserId());
            statement.setLong(2, borrow.getBookId());
            statement.setDate(3, new java.sql.Date(new java.util.Date().getTime()));
            statement.setDate(4, new java.sql.Date(new java.util.Date().getTime() + 518_400_000));
//            System.out.println(statement);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public BorrowInfo search(Long id) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(searchQuery);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                long borrowId = rs.getInt("id");
                long userId = rs.getLong("user_id");
                long bookId = rs.getLong("book_id");
                Date start = rs.getDate("start");
                Date end = rs.getDate("end");
                Date returnDate = rs.getDate("return_date");
                long penalty = rs.getLong("penalty");


                BorrowInfo borrowInfo = new BorrowInfo(userId, bookId, start, end, returnDate, penalty);
                borrowInfo.setId(borrowId);
                return borrowInfo;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int update(BorrowInfo borrowInfo) {

        try {
            PreparedStatement statement = getConnection().prepareStatement(updateQuery);
            statement.setLong(1, borrowInfo.getUserId());
            statement.setLong(2, borrowInfo.getBookId());
            statement.setDate(3, (Date) borrowInfo.getStart());
            statement.setDate(4, (Date) borrowInfo.getEnd());
            statement.setDate(5, (Date) borrowInfo.getReturnDate());
            statement.setLong(6, borrowInfo.getPenalty());
            statement.setLong(7, borrowInfo.getId());

//            System.out.println(statement);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delete(Long id) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(deleteQuery);
            statement.setLong(1, id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Collection<BorrowInfo> findAll() {
        Collection<BorrowInfo> borrowInfos = new LinkedList<>();
        try {
            PreparedStatement statement = getConnection().prepareStatement(selectAllQuery);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                long borrowId = rs.getInt("id");
                long userId = rs.getLong("user_id");
                long bookId = rs.getLong("book_id");
                Date start = rs.getDate("start");
                Date end = rs.getDate("end");
                Date returnDate = rs.getDate("return_date");
                long penalty = rs.getLong("penalty");

                BorrowInfo borrowInfo = new BorrowInfo(userId, bookId, start, end, returnDate, penalty);
                borrowInfo.setId(borrowId);
                borrowInfos.add(borrowInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrowInfos;
    }

    public BorrowInfo findLast(long id) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(selectAllWithIsbnQuery);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                long borrowId = rs.getInt("id");
                long userId = rs.getLong("user_id");
                long bookId = rs.getLong("book_isbn");
                Date start = rs.getDate("start");
                Date end = rs.getDate("end");
                Date returnDate = rs.getDate("return_date");
                long penalty = rs.getLong("penalty");
                BorrowInfo borrowInfo= new BorrowInfo(userId, bookId, start, end, returnDate, penalty);
                borrowInfo.setId(borrowId);
                return borrowInfo;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int custem(String query) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
