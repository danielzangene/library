package repository.dao;

import model.Book;
import model.User;
import repository.connection.MySQLConnection;

import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;

public class BookDAO extends AbstractDAO<Book,Long> {

    public static BookDAO INSTANCE = new BookDAO();

    private String insertQuery = "INSERT INTO `books`(`id`, `title`, `subject`, `isbn`, `status`, `price`, `penalty`)" +
            " VALUES (NULL,?,?,?,?,?,?)";
    private String searchQuery = "select * from books where isbn =?";
    private String updateQuery = "UPDATE `books` SET  `title`=?, `subject`=?, `isbn`=?, `status`=?, `price`=?, `penalty`=? WHERE isbn=?";
    private String deleteQuery = "DELETE FROM `books` WHERE isbn=?";
    private String selectAllQuery = "select * from books ";

    private BookDAO() {
    }

    static {
        INSTANCE.setConnection(MySQLConnection.getCONNECTION());
    }

    @Override
    public int insert(Book book) {

        try {
            PreparedStatement statement = getConnection().prepareStatement(insertQuery);
            statement.setString(1, book.getTitle());
            statement.setString(2, book.getSubject());
            statement.setLong(3, book.getIsbn());
            statement.setDate(4, book.getStatus());
            statement.setLong(5, book.getPrice());
            statement.setLong(6, book.getPenaltyFee());
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Book search(Long id) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(searchQuery);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
//                int bookId = rs.getInt("id");
                String title = rs.getString("title");
                String subject = rs.getString("subject");
                Date status = rs.getDate("status");
                long price = rs.getLong("price");
                long isbn = rs.getLong("isbn");
                long penalty = rs.getLong("penalty");

                return new Book(title, subject, isbn, status, price, penalty);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int update(Book book) {

        try {
            PreparedStatement statement = getConnection().prepareStatement(updateQuery);
            statement.setString(1, book.getTitle());
            statement.setString(2, book.getSubject());
            statement.setLong(3, book.getIsbn());
            statement.setDate(4, book.getStatus());
            statement.setLong(5, book.getPrice());
            statement.setLong(6, book.getPenaltyFee());
            statement.setLong(7, book.getIsbn());
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delete(Long id) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(deleteQuery);
            statement.setLong(1, id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Collection<Book> findAll() {
        Collection<Book> books = new LinkedList<>();
        try {
            PreparedStatement statement = getConnection().prepareStatement(selectAllQuery);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
//                int bookId = rs.getInt("id");
                String title = rs.getString("title");
                String subject = rs.getString("subject");
                Date status = rs.getDate("status");
                long price = rs.getLong("price");
                long isbn = rs.getLong("isbn");
                long penalty = rs.getLong("penalty");

                books.add(new Book(title, subject, isbn, status, price, penalty));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public int custem(String query) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
