package repository.dao;

import model.User;
import repository.connection.MySQLConnection;
import util.Hash;

import java.sql.*;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

public class UserDAO extends AbstractDAO<User, String> {

    public static UserDAO INSTANCE = new UserDAO();

    private String insertQuery = "INSERT INTO `users`(`id`, `full_name`, `email`, `password`, `created_at`, `national_code`," +
            " `bodget`,`status`) VALUES (NULL ,?,?,?,?,?,?,?)";
    private String searchQuery = "select * from users where email =?";
    private String updateQuery = "UPDATE `users` SET `full_name`=?,`bodget`=?,`incorrect_pass`=?,`get_late`=?,`status`=?,`password`=? WHERE email=?";
    private String deleteQuery = "DELETE FROM `users` WHERE email=?";
    private String selectAllQuery = "select * from users ";

    static {
        INSTANCE.setConnection(MySQLConnection.getCONNECTION());
    }

    private UserDAO() {
    }


    @Override
    public int insert(User user) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(insertQuery);
            statement.setString(1, user.getName());
            statement.setString(2, user.getEmail());
            statement.setString(3, (user.getPassword()));
            statement.setTimestamp(4, new Timestamp(new java.util.Date().getTime()));
            statement.setString(5, user.getNationalCode());
            statement.setLong(6, user.getBudget());
            statement.setDate(7, null);
//            System.out.println(statement);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public User search(String id) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(searchQuery);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int userId = rs.getInt("id");
                String name = rs.getString("full_name");
                Timestamp created_at = rs.getTimestamp("created_at");
                String national_code = rs.getString("national_code");
                java.sql.Date status = rs.getDate("status");
                long budget = rs.getLong("bodget");
                byte incorrectPass = rs.getByte("incorrect_pass");
                byte getLate = rs.getByte("get_late");
                String pass = rs.getString("password");
                String email = rs.getString("email");
                User user = new User(name, national_code, created_at, pass, email,status , budget,incorrectPass,getLate);
                user.setId(userId);
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int update(User user) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(updateQuery);
            statement.setString(1, user.getName());
            statement.setLong(2, user.getBudget());
            statement.setInt(3, user.getIncorrectPass());
            statement.setInt(4, user.getGetLate());
            statement.setDate(5,user.getStatus());
            statement.setString(6,user.getPassword());
            statement.setString(7, user.getEmail());
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delete(String id) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(deleteQuery);
            statement.setString(1, id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Collection<User> findAll() {
        Collection<User> users = new LinkedList<>();
        try {
            PreparedStatement statement = getConnection().prepareStatement(selectAllQuery);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int userId = rs.getInt("id");
                String name = rs.getString("full_name");
                Timestamp created_at = rs.getTimestamp("created_at");
                String national_code = rs.getString("national_code");
                java.sql.Date status = rs.getDate("status");
                long budget = rs.getLong("bodget");
                byte incorrectPass = rs.getByte("incorrect_pass");
                byte getLate = rs.getByte("get_late");
                String pass = rs.getString("password");
                String email = rs.getString("email");
                User user = new User(name, national_code, created_at, pass, email,status , budget,incorrectPass,getLate);
                user.setId(userId);
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public int custem(String query) {
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
