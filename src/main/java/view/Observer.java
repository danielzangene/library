package view;

import model.Observable;

public interface Observer<T extends Observable> {
    void update(T t);
}
