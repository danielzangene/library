package controller;

import model.Book;
import model.BorrowInfo;
import model.User;
import repository.LibraryRepository;
import util.DateUtil;
import util.ScannerWrapper;

import java.sql.Date;

public class BorrowController {
    protected static void borrowBook(User user) {
        BookController.bookList();
        System.out.print("enter isbn number: ");
        long ser = ScannerWrapper.getLong();
        Book book = LibraryRepository.bookRepo.search(ser);
        if (book.getStatus() != null && book.getStatus().getTime() > new java.util.Date().getTime()) {
            System.out.println("-" + book.getTitle() + "- is not available!");
        } else {
            System.out.print("do you want " + book.getTitle() + "? (y/n)");
            String str = ScannerWrapper.getLine();
            if (str.toLowerCase().charAt(0) == 'y') {
                book.setStatus(new Date(new java.util.Date().getTime() + 432_000_000));
                LibraryRepository.bookRepo.update(book);
                BorrowInfo info = new BorrowInfo(user.getId(), book.getIsbn());
                LibraryRepository.borrowInfoRepo.insert(info);
            }
        }
    }

    protected static void returnBook(User user) {

        System.out.print("enter the books isbn: ");
        long isbn = ScannerWrapper.getLong();
        BorrowInfo borrowInfo = LibraryRepository.borrowInfoRepo.findLast(isbn);
        if (borrowInfo != null) {
            if (borrowInfo.getReturnDate() == null) {
                if (borrowInfo.getEnd().getTime() >= DateUtil.getNowDateTime()) {
                    borrowInfo.setReturnDate(new Date(DateUtil.getNowDateTime()));
                    Book book = LibraryRepository.bookRepo.search(borrowInfo.getBookId());
                    LibraryRepository.borrowInfoRepo.update(borrowInfo);
                    book.setStatus(null);
//                    System.out.println(book);
                    LibraryRepository.bookRepo.update(book);
                } else {
                    int delay = (int) Math.ceil(Math.abs(borrowInfo.getEnd().getTime() - DateUtil.getNowDateTime()) / 86_400_000d);
                    borrowInfo.setReturnDate(new Date(DateUtil.getNowDateTime()));
                    Book book = LibraryRepository.bookRepo.search(borrowInfo.getBookId());
                    borrowInfo.setPenalty(delay * book.getPenaltyFee());
                    LibraryRepository.borrowInfoRepo.update(borrowInfo);
                    book.setStatus(null);
                    LibraryRepository.bookRepo.update(book);
                    user.setBudget(user.getBudget() - (delay * book.getPenaltyFee()));

                    if (user.getGetLate() < 1) {
                        user.setGetLate((byte) (user.getGetLate() + 1));
                        LibraryRepository.userRepo.update(user);
                    } else {
                        user.setStatus(new Date(DateUtil.getNowDateTime() + 604_800_000));
                        user.setGetLate((byte) 0);
                        LibraryRepository.userRepo.update(user);
                        System.out.println("youre suspend for a week!");
                        return;
                    }
                }
            } else {
                System.out.println("book is in the library");
            }
        } else {
            if (LibraryRepository.bookRepo.search(isbn) != null)
                System.out.println("book is in the library");
            else
                System.out.println("book not exist");

        }
    }
}
