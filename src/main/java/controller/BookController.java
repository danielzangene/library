package controller;

import model.Book;
import model.User;
import repository.LibraryRepository;

import java.util.Collection;

public class BookController {
    protected static void bookList() {
        Collection<Book> books = LibraryRepository.bookRepo.findAll();
        books.stream().forEach(x -> System.out.print("(" + x.getIsbn() + ") " + x.getTitle() +
                (x.getStatus() != null && x.getStatus().getTime() > new java.util.Date().getTime() ? "--not available" : "--available") + "\n"));
    }
}
