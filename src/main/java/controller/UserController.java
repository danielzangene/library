package controller;

import model.Book;
import model.User;
import repository.LibraryRepository;
import util.DateUtil;
import util.Hash;
import util.ScannerWrapper;
import java.sql.Date;

public class UserController {
    public static void SignUp() {
        System.out.print("-----SignUp\nname: ");
        String name = ScannerWrapper.getLine();
        System.out.print("email: ");
        String email = ScannerWrapper.getLine();
        System.out.print("password: ");
        String pass = ScannerWrapper.getLine();
        System.out.print("national code: ");
        String national_code = ScannerWrapper.getLine();
        System.out.print("bodget: ");
        long bodget = ScannerWrapper.getLong();
        User user = new User(name, national_code, null, Hash.getHash(pass), email, null, bodget);
        LibraryRepository.userRepo.insert(user);
    }

    public static void login() {
        System.out.println("-----Login");
        System.out.print("email: ");
        String email = ScannerWrapper.getLine();
        System.out.print("password: ");
        String pass = ScannerWrapper.getLine();
        User user = LibraryRepository.userRepo.search(email);
        if (user != null) {
            if (user.getStatus() != null && user.getStatus().getTime() > new java.util.Date().getTime()) {
                System.out.println("user suspended");
            } else if (user.getPassword().equals(Hash.getHash(pass))) {
                System.out.println("youre loged in!");
                userPanel(user);
            } else {
                System.out.println("incorrect password");
                if (user.getIncorrectPass() < 2) {
                    user.setIncorrectPass((byte) (user.getIncorrectPass() + 1));
                    LibraryRepository.userRepo.update(user);
                } else {
                    user.setStatus(new Date(new java.util.Date().getTime() + 86_400_000));
                    user.setIncorrectPass((byte) 0);
                    LibraryRepository.userRepo.update(user);
                    System.out.println("youre suspend");
                }
            }
        } else {
            System.out.println("user not found!");
        }
    }

    private static void userPanel(User user){
        while (user.getStatus()==null||user.getStatus().getTime()< DateUtil.getNowDateTime()) {
            System.out.print("--->Library\n" +
                    "1.borrow a book.\n" +
                    "2.return.\n" +
                    "3.log out.\n"+
                    "num ?");
            int item =ScannerWrapper.getInt();
            switch (item) {
                case 1:
                    BorrowController.borrowBook(user);
                    break;
                case 2:
                    BorrowController.returnBook(user);
                    break;
                case 3:
                    return;
            }
        }
    }
}
